import { Component } from '@angular/core';
import { Movie } from '../models/movie.model';
import { MOVIES } from '../../../data/movies-data';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-movies',
  templateUrl: './main-movies.component.html',
  styleUrls: ['./main-movies.component.css'],
})
export class MainMoviesComponent {
  constructor(private router: Router) {}
  movies: Movie[] = MOVIES;

  watchlist: number[] = JSON.parse(localStorage.getItem('watchlist') || '[]');

  sortingOptions = [
    { label: 'Sort by Title', value: 'title' },
    { label: 'Sort by Release Date', value: 'releaseDate' },
  ];

  sortOption: any = {
    value: 'title',
  };

  onChangeSortOption() {
    console.log('Selected sorting option:', this.sortOption);
  }

  addToWatchlist(movie: Movie): void {
    console.log(`Added ${movie.title} to the watchlist`);
    const movieId = movie.id;

    const index = this.watchlist.indexOf(movieId);
    if (index !== -1) {
      this.watchlist.splice(index, 1);
    } else {
      this.watchlist.push(movieId);
    }
    localStorage.setItem('watchlist', JSON.stringify(this.watchlist));
  }

  isAddedToWatchlist(movie: Movie): boolean {
    return this.watchlist.includes(movie.id);
  }

  goToMovieDetails(movieId: number): void {
    console.log('Navigating to movie details:', movieId);
    this.router.navigate(['movies', movieId]);
  }

  sortMovies(): void {
    console.log(this.sortOption.value);
    if (this.sortOption.value === 'title') {
      this.movies.sort((a, b) => a.title.localeCompare(b.title));
    } else if (this.sortOption.value === 'releaseDate') {
      this.movies.sort(
        (a, b) =>
          new Date(a.releasedDate).getTime() -
          new Date(b.releasedDate).getTime()
      );
    }
  }
}
