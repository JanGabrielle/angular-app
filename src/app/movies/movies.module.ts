import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WatchlistModule } from '../watchlist/watchlist.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, WatchlistModule],
})
export class MoviesModule {}
