import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../movies/models/movie.model';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrl: './watchlist.component.css',
})
export class WatchlistComponent {
  @Input() isAddedToWatchlist: boolean | undefined;
  @Output() addToWatchlist = new EventEmitter<void>();
}
