import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routes';
import { MoviesModule } from './movies/movies.module';
import { MainMoviesComponent } from './movies/main-movies/main-movies.component';
import { MovieDetailsComponent } from './movie-details/movie-details/movie-details.component';
import { WatchlistComponent } from './watchlist/watchlist/watchlist.component';

import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    MainMoviesComponent,
    MovieDetailsComponent,
    WatchlistComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CardModule,
    ButtonModule,
    MoviesModule,
    FormsModule,
    DropdownModule,
    BrowserAnimationsModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
