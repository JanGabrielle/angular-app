import { NgModule } from '@angular/core';
import { MovieDetailsComponent } from './movie-details/movie-details/movie-details.component';
import { Routes, RouterModule } from '@angular/router';
import { MainMoviesComponent } from './movies/main-movies/main-movies.component';

export const routes: Routes = [
  { path: '', redirectTo: '/movies', pathMatch: 'full' },
  { path: 'movies', component: MainMoviesComponent },
  { path: 'movies/:id', component: MovieDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
