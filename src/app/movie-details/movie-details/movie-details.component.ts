import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Movie } from '../../movies/models/movie.model';
import { MOVIES } from '../../../data/movies-data';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrl: './movie-details.component.css',
})
export class MovieDetailsComponent {
  movie: Movie | undefined;
  videoId: string | undefined;

  watchlist: number[] = JSON.parse(localStorage.getItem('watchlist') || '[]');

  constructor(private route: ActivatedRoute, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      const movieId = params['id'];
      console.log(movieId);
      this.movie = MOVIES.find((m) => m.id == movieId);

      if (this.movie && this.movie.trailerLink) {
        this.videoId = this.extractVideoId(this.movie.trailerLink);
        console.log(this.videoId);
      }
    });
  }

  private extractVideoId(trailerLink: string): string | undefined {
    const videoIdMatch = trailerLink.match(
      /(?:youtu\.be\/|youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/|y\/|\/v\/|\/e\/|watch\?v=|&v=)([^"&?\/\s]{11})/
    );

    return videoIdMatch ? videoIdMatch[1] : undefined;
  }
  getEmbedUrl(): SafeResourceUrl | undefined {
    if (this.videoId) {
      // Sanitize the URL
      const url = `https://www.youtube.com/embed/${this.videoId}`;
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
    return undefined;
  }

  addToWatchlist(movie: Movie): void {
    if (movie) {
      console.log(`Added ${movie.title} to the watchlist`);
      const movieId = movie.id;

      const index = this.watchlist.indexOf(movieId);
      if (index !== -1) {
        this.watchlist.splice(index, 1);
      } else {
        this.watchlist.push(movieId);
      }
      localStorage.setItem('watchlist', JSON.stringify(this.watchlist));
    }
  }

  isAddedToWatchlist(movie: Movie | undefined): boolean {
    return !!movie && this.watchlist.includes(movie.id);
  }
}
